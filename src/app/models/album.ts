// ng g i models/album

export interface Album {
  id: string;
  name: string;
  artists: Artist[];
  images: AlbumImage[];
}

export interface Artist {}
export interface AlbumImage {
  height: number;
  width: number;
  url: string;
}

export interface PagingObject<T> {
  items: T[];
  limit: number;
  offset: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
