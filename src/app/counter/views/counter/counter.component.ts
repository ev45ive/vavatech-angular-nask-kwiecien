import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../../../reducers';
import { Increment, Decrement, Reset } from '../../counter.actions';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  counter$ = this.store.pipe(
    // select(s => s.counter)
    select('counter')
  )

  constructor(private store: Store<State>) { }

  inc(amount:string = '1'){
    this.store.dispatch( new Increment(parseInt(amount)))  
  }
  
  dec(){
    this.store.dispatch( new Decrement())
  }

  reset(){
    this.store.dispatch( new Reset())
  }

  ngOnInit() {
  }

}
