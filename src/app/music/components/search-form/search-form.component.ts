import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormArray,
  Validators,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  filter,
  distinct,
  distinctUntilChanged,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";
import { Observable } from "rxjs/internal/Observable";
import { Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  censor = (badword): ValidatorFn => (
    control: AbstractControl
  ): ValidationErrors | null => {
    //
    //

    const hasError = (control.value as string).includes(badword);

    return hasError
      ? {
          censor: { badword: badword }
        }
      : null;
  };

  asyncCensor = (badword): AsyncValidatorFn => (
    //
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    // return this.http.post('validato',value).pipe(map(resp=>resp.error))

    return Observable.create((observer: Observer<ValidationErrors | null>) => {
      // onSubscribe:
      const handler = setTimeout(() => {
        const hasError = (control.value as string).includes(badword);

        observer.next(
          hasError
            ? {
                censor: { badword: badword }
              }
            : null
        );
        observer.complete();
      }, 1000);

      // onUnsubscribe:
      return () => {
        clearTimeout(handler);
      };
    });
  };

  @Input()
  set query(query){
    this.queryForm.get('query').setValue(query,{
      emitEvent: false,
      // onlySelf:true
    })
  }

  queryForm = new FormGroup({
    query: new FormControl(
      "",
      [
        Validators.required,
        Validators.minLength(3)
        // this.censor("batman"),
      ],
      // [this.asyncCensor("batman")]
    )
  });

  constructor() {
    const queryField = this.queryForm.get("query");

    const valueChanges = queryField.valueChanges;

    const validChanges = queryField.statusChanges.pipe(
      filter(status => status == "VALID")
    );

    const searchChanges = validChanges.pipe(
      withLatestFrom(valueChanges, (valid, value) => value),
      debounceTime(400),
      distinctUntilChanged(),
      filter(query => query.length >= 3)
    );

    searchChanges.subscribe(query => {
      this.search(query);
    });

    console.log(this.queryForm);
  }

  ngOnInit() {}

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);
  }
}
