import { Directive } from '@angular/core';
import { MusicSearchService } from './services/music-search.service';

@Directive({
  selector: '[appMusicSearchProvider]',
  providers:[
    MusicSearchService
  ]
})
export class MusicSearchProviderDirective {

  constructor() { }

}
