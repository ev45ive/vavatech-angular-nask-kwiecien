import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchComponent } from "./views/music-search/music-search.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { AlbumsListComponent } from "./components/albums-list/albums-list.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import { API_URL_TOKEN } from "./tokens";

import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';
import { MusicSearchProviderDirective } from './music-search-provider.directive';

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsListComponent,
    AlbumCardComponent,
    MusicSearchProviderDirective
  ],
  imports: [
    CommonModule,     
    MusicRoutingModule, 
    // HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    // {
    //   provide: API_URL_TOKEN,
    //   useValue: environment.api_url
    // }
    // {
    //   provide: MusicSearchService,
    //   useFactory: (api_url: string) => {
    //     return new MusicSearchService(api_url);
    //   },
    //   deps: [API_URL_TOKEN]
    // },
    // {
    //   provide: AbstactSearchService,
    //   useClass: SpotifySearchService,
    //   // deps: [API_URL_TOKEN]
    // },
    // MusicSearchService
  ],
  exports: [MusicSearchComponent, MusicSearchProviderDirective]
})
export class MusicModule {}
