import { Injectable, Inject } from "@angular/core";
import { Album, AlbumsResponse } from "../../models/album";
import { API_URL_TOKEN } from "../tokens";
import { HttpClient } from "@angular/common/http";

import {
  map,
  switchMap,
  catchError
} from "rxjs/operators";
import {
  ReplaySubject,
  BehaviorSubject,
  empty
} from "rxjs";

@Injectable({
  // providedIn: MusicModule
  providedIn: "root"
})
export class MusicSearchService {
  // queryChange = new BehaviorSubject<string>('batman');

  private albumsChange = new BehaviorSubject<Album[]>([]);
  private queryChange = new ReplaySubject<string>(1);
  // private latestError = new ReplaySubject<Error>(1);

  constructor(
    @Inject(API_URL_TOKEN)
    private api_url: string,
    private http: HttpClient
  ) {
    this.queryChange
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.requestAlbums(params) // + handle errors:
          .pipe(catchError(() => empty()))
        )
      )
      .subscribe(albums => {
        this.albumsChange.next(albums);
      });
  }

  requestAlbums(params) {
    return this.http
      .get<AlbumsResponse>(this.api_url, {
        params
      })
      .pipe(map(resp => resp.albums.items));
  }

  getAlbums() {
    return this.albumsChange.asObservable();
  }

  getQuery() {
    return this.queryChange.asObservable();
  }

  search(query: string): void {
    console.log('saerch',query);
    
    this.queryChange.next(query);
  }

  ngOnDestroy(){
    console.log('MusicSearchService says bye bye!')
  }
}
