import { TestBed } from "@angular/core/testing";

import { MusicSearchService } from "./music-search.service";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { API_URL_TOKEN } from "../tokens";
import { Album } from '../../models/album';

fdescribe("MusicSearchService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: API_URL_TOKEN,
          useValue: "PLACKI"
        }
      ]
    })
  );

  let service: MusicSearchService;
  beforeEach(() => {
    service = TestBed.get(MusicSearchService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should request albums from server", (done) => {
    const service: MusicSearchService = TestBed.get(MusicSearchService);
    const httpCtrl: HttpTestingController = TestBed.get(HttpTestingController);
    const api_url: string = TestBed.get(API_URL_TOKEN);

    service.search("batman");

    httpCtrl.expectOne(
      req => req.url == api_url && req.params.get("q") == "batman",
      "albums search url with query 'batman' "
    ).flush({
      albums:{ items: ['placki']}
    })

    service.getAlbums().subscribe((albums)=>{
      expect(albums).toEqual(['placki'] as any)
      done()
    });
  });
});
