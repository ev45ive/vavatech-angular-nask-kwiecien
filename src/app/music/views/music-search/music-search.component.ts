import { Component, OnInit } from "@angular/core";
import { MusicSearchService } from "../../services/music-search.service";
import { empty } from "rxjs";
import { tap, catchError, share } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.scss"]
})
export class MusicSearchComponent implements OnInit {
  query$ = this.service.getQuery();

  albums$ = this.service.getAlbums().pipe(
    catchError(error => (this.error = error) && empty()),
    share()
  );
  error: Error;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {}

  search(query: string) {
    // this.service.search(query);
    this.router.navigate(["/music"], {
      queryParams: {
        q: query
      },
      replaceUrl: true
    });
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(queryParamMap => {
      const q = queryParamMap.get("q");
      if (q) {
        this.service.search(q);
      }
    });
  }
}

// {show? <Async obs={obs.pipe(...)}>
// {  (data) => <MyComponent data={data} />}
// </Async> : ''}
