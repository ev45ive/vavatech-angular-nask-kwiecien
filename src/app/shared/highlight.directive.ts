import {
  Directive,
  ElementRef,
  Attribute,
  Input,
  OnInit,
  DoCheck,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host:{
  //   '[style.borderColor]':'color',
  //   '(mouseenter)':'activate($event)'
  // }
})
export class HighlightDirective
  implements OnInit, DoCheck, OnChanges, OnDestroy {
  @Input("appHighlight")
  color: string;

  @HostBinding("style.color")
  @HostBinding("attr.data-color")
  get currentColor(){
    return this.hover? this.color: ''
  }

  hover = false;

  @HostListener("mouseenter", ["$event.x"])
  activate(x: number) {
    this.hover = true;
  }

  @HostListener("mouseleave", ["$event.x"])
  deactivate(x: number) {
    this.hover = false;
  }

  constructor(private elem: ElementRef<HTMLElement>) {
    // console.log("constructor", this.color);
  }

  ngOnInit() {
    // console.log("ngOnInit!", this.color);
  }

  ngDoCheck(): void {
    // console.log("ngDoCheck");
  }

  // Shallow compare inputs
  ngOnChanges(changes: SimpleChanges): void {
    // console.log("ngOnChanges", changes);

    // this.elem.nativeElement.style.color = this.color;
  }

  ngOnDestroy(): void {
    // console.log("ngOnDestroy");
  }
}

// console.log(HighlightDirective)