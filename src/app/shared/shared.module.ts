import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './highlight.directive';
import { CardComponent } from './card/card.component';

@NgModule({
  declarations: [HighlightDirective, CardComponent],
  imports: [
    CommonModule
  ],
  exports: [HighlightDirective, CardComponent]
})
export class SharedModule { }
