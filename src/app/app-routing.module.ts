import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists/views/playlists-view/playlists-view.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "playlists",
    children: [
      {
        path: "",
        component: PlaylistsViewComponent
      },
      {
        path: ":id",
        component: PlaylistsViewComponent
      }
    ]
  },
  {
    path:'music',
    loadChildren:'./music/music.module#MusicModule'
  },
  {
    path: "**",
    redirectTo: "playlists",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // enableTracing: true
      // useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
