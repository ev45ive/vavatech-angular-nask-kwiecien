import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ApplicationRef } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { MusicModule } from './music/music.module';
import { SecurityModule } from './core/security/security.module';
import { API_URL_TOKEN } from './music/tokens';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { CounterModule } from './counter/counter.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    PlaylistsModule,
    // MusicModule,
    HttpClientModule,
    SecurityModule,
    CounterModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [
    {
      provide: API_URL_TOKEN,
      useValue: environment.api_url
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private app:ApplicationRef){}
  // ngDoBootstrap(){
  //   // fetchConfigFromServer().then( =>
  //   this.app.bootstrap(AppComponent)
  // }
}
