import { Injectable } from "@angular/core";
import { AuthOptions } from "./auth-options";
import { HttpParams } from "@angular/common/http";

/* 
Read me:
https://angular.io/api/common/http/HttpClientXsrfModule
*/

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string = null;

  constructor(private options: AuthOptions) {}

  authorize() {
    const { auth_url, client_id, response_type, redirect_uri } = this.options;

    const p = new HttpParams({
      fromObject: {
        client_id,
        response_type,
        redirect_uri
      }
    });
    // console.log(`${auth_url}?${p}`);
    localStorage.removeItem('token')
    location.href = `${auth_url}?${p}`;
  }

  getToken() {
    this.token = JSON.parse(localStorage.getItem('token'))

    if (location.hash && !this.token) {
      const p = new HttpParams({
        fromString: location.hash
      });
      this.token = p.get("#access_token");
      localStorage.setItem('token', JSON.stringify(this.token))
      location.hash = ''
    }

    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
