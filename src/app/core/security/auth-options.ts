export class AuthOptions {
  client_id: string;
  response_type: string;
  redirect_uri: string;
  auth_url: string;
}
