import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { counterReducer } from '../counter/counter.reducer';
import { playlistsInitialState, playlists } from '../playlists/playlists.reducer';

export interface State {
  counter: number,
  playlists: typeof playlistsInitialState
}

export const reducers: ActionReducerMap<State> = {
  counter: counterReducer,
  playlists: playlists
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
