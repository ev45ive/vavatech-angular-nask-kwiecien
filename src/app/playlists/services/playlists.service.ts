import { Injectable } from "@angular/core";
import { Playlist } from "../../models/playlist";
import { Observable, of, BehaviorSubject } from "rxjs";
import { map, distinctUntilChanged } from "rxjs/operators";
import { Store, select } from "@ngrx/store";
import { State } from "../../reducers";
import { UpdatePlaylist, AddPlaylist } from "../playlists.actions";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  createNew(): any {
    this.store.dispatch(
      new AddPlaylist({
        id: Date.now(),
        name: "" + Date.now(),
        color: "#ffff00",
        favourite: false
      })
    );
  }
  /* 
  private playlists = new BehaviorSubject([
    {
      id: 123,
      name: "Angular Hits",
      favourite: false,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular TOP20",
      favourite: true,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "The best of Angular ",
      favourite: false,
      color: "#00ffff"
    }
  ]);
 */

  constructor(private store: Store<State>) {}
  playlists = this.store.pipe(select(s => s.playlists.items));

  save(draft: Playlist) {
    this.store.dispatch(new UpdatePlaylist(draft));
  }

  getPlaylist(id: number) {
    return this.playlists.pipe(
      map(playlists => playlists.find(p => p.id == id)),
      distinctUntilChanged()
    );
  }

  getPlaylists() {
    return this.playlists;
  }
}
