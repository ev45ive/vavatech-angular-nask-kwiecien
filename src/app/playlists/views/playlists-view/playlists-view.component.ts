import { Component, OnInit } from "@angular/core";
import { Playlist } from "../../../models/playlist";
import { PlaylistsService } from "../../services/playlists.service";
import { Router, ActivatedRoute } from "@angular/router";
import { map, filter, switchMap, share } from "rxjs/operators";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent implements OnInit {
  playlists$ = this.service.getPlaylists();

  selected$ = this.route.paramMap.pipe(
    map(paramMap => parseInt(paramMap.get("id"))),
    filter(Boolean),
    switchMap(id => this.service.getPlaylist(id)),
    share()
  );

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {}

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id], {
      replaceUrl: true
    });
  }

  ngOnInit() {}

  save(draft: Playlist) {
    this.service.save(draft);
  }

  createNew(){
    this.service.createNew()
  }
}
