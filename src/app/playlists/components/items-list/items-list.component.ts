import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from "@angular/core";
import { Playlist } from "../../../models/playlist";
import { NgForOf, NgForOfContext } from "@angular/common";

NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"],
  // inputs:['playlists:items']
  // encapsulation: ViewEncapsulation.Emulated
})
export class ItemsListComponent implements OnInit {

  @Input('items')
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist;

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist)
  }

  constructor() {}

  ngOnInit() {}

  trackFn(index: number, item: Playlist) {
    return item.id;
  }
}
