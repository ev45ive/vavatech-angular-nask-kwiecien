import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  AfterViewChecked
} from "@angular/core";
import { Playlist } from "../../../models/playlist";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist;

  random() {
    return Math.random();
  }

  mode: "show" | "edit" = "show";

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @Output()
  playlistSave = new EventEmitter();

  save(form: NgForm) {
    const draft = {
      ...this.playlist,
      ...form.value
    };
    this.playlistSave.emit(draft);
  }

  constructor() {}

  ngOnInit() {}
}
