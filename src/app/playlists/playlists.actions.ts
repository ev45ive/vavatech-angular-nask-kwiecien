import { Action, ActionCreator, Creator } from "@ngrx/store";
import { Playlist } from "../models/playlist";

export enum ActionTypes {
  AddPlaylist = "[Playlists] Add",
  RemovePlaylist = "[Playlists] Remove",
  UpdatePlaylist = "[Playlists] Update",
  LoadPlaylists = "[Playlists] Load from server"
}

export class AddPlaylist implements Action {
  readonly type = ActionTypes.AddPlaylist;
  constructor(readonly payload: Playlist) {}
}

export class RemovePlaylist implements Action {
  readonly type = ActionTypes.RemovePlaylist;
  constructor(readonly payload: Playlist["id"]) {}
}

export class UpdatePlaylist implements Action {
  readonly type = ActionTypes.UpdatePlaylist;
  constructor(readonly payload: Playlist) {}
}

export class LoadPlaylists implements Action {
  readonly type = ActionTypes.LoadPlaylists;
  constructor(readonly payload: Playlist[]) {}
}
