import {
  AddPlaylist,
  RemovePlaylist,
  UpdatePlaylist,
  ActionTypes
} from "./playlists.actions";
import { Playlist } from "../models/playlist";
import { Action } from "@ngrx/store";

export const playlistsInitialState = {
  items: [
    {
      id: 123,
      name: "Angular Hits",
      favourite: false,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular TOP20",
      favourite: true,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "The best of Angular ",
      favourite: false,
      color: "#00ffff"
    }
  ] as Playlist[],
  message: "placki"
};

export function playlists(state = playlistsInitialState, action: any) {
  // debugger;
  switch (action.type) {
    case ActionTypes.AddPlaylist:
      return { ...state, items: [...state.items, action.payload] };
    case ActionTypes.RemovePlaylist:
      return {
        ...state,
        items: [...state.items.filter(p => p !== action.payload)]
      };
    case ActionTypes.UpdatePlaylist:
      return {
        ...state,
        items: [
          ...state.items.map(p =>
            p.id == action.payload.id ? action.payload : p
          )
        ]
      };
    default:
      return state;
  }
}
