import { AuthOptions } from '../app/core/security/auth-options';

export const environment = {
  production: true,
  
  api_url: "https://api.spotify.com/v1/search",

  auth_config: {
    auth_url: "https://accounts.spotify.com/authorize",
    response_type: "token",
    redirect_uri: "http://localhost:4200/",
    client_id: "9c9c470709b84b17a6e457b1575ddd3b",
  } as AuthOptions
};
